package com.mysaarthi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MySaarthi extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysaarthi_main);

        Intent i = new Intent();
        i.setClassName("com.mysaarthi.common_utils.ui",
                "com.mysaarthi.common_utils.ui.home");
        startActivity(i);
    }
}
